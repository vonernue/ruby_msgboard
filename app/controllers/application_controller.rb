class ApplicationController < ActionController::Base

    protected
    def authenticate_admin
        unless current_user.admin?
            flash[:alert] = "You cannot perform this action"
            redirect_to root_path
        end
    end
end
