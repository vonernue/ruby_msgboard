class MessagesController < ApplicationController
    before_action :authenticate_user!, :except => [:index]
    before_action :authenticate_admin, :only => [:destroy]
    def index
        @messages = Message.order(created_at: :desc).page(params[:page]).per(5)
        # @messages = @messages.sort_by { |m| m.created_at }.reverse
    end

    def new
        @message = Message.new
    end

    def create
        @message = Message.new(message_params)
        @message.save
        redirect_to messages_path
    end 

    def destroy
        @message = Message.find(params[:id])
        @message.destroy
        flash[:notice] = "Message Deleted."
        redirect_to messages_path
    end

    private

    def message_params
        params.require(:message).permit(:author, :msg)
    end

end
